from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from todos.models import (TodoItem, TodoList)

class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todo"
    
class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "detail"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/todoitem/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self, **kwargs):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/todoitem/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self, **kwargs):
        return reverse_lazy("todo_list_detail",  args=[self.object.id])