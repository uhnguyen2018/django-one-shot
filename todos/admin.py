from django.contrib import admin
from todos.models import TodoList, TodoItem

class TodoItemAdmin(admin.ModelAdmin):
    pass

class TodoListAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoList, TodoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
